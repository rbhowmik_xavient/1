#include <iostream>

using namespace std;

class some{
    public:
    int t;
    some(){
        t=77;
    }
    void destroy(){
        delete this;
    }
};

int main()
{  
   some *s;s=new some();
   cout<<s->t;
   s->destroy();//runtime err as this == &s
   cout<<s->t;
   some r;
   cout<<r.t;
   r.destroy();//no runtime err as this == s
   cout<<r.t; 
   return 0;
}